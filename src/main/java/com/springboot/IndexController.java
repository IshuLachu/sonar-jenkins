package com.springboot;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
@RequestMapping("/")
public String home(Map<String,Object> model)
{
	model.put("message","WELCOME TO HCL ");
	return "index";
}
@RequestMapping("/next")
public String next(Map<String,Object> model)
{
	model.put("message","YOU ARE IN MYHCL ");
	return "next";
}

@Value("${welcome.message}")
private String messages = "";

                @RequestMapping("/msg")
                public String welcome(Map<String, Object> model) {
                                model.put("message",messages);
                                return "welcome";
                }

}
